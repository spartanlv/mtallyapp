import AgentsService from '../../services/agents.service';
export class Controller {
  all(req, res) {
    AgentsService
    .all(req)
    .then(r => res.json(r))
    .catch(error => {
      console.log(error);
      res.status(error.status || 500).end(error.message)
    });
  }

  byId(req, res) {
    AgentsService
      .byId(req.params.idNo)
      .then(r => {
        if (r) res.json(r)
        else res.status(404).end();
      })
      .catch(error => {
        console.log(error);
        res.status(error.status || 500).end(error.message)
      });
  }

  create(req, res) {
    AgentsService
      .create(req)
      .then(r => {
        if (r) {
          res
            .status(201)
            .json(r)
        }
        })
      .catch(error => {
        console.log(error.message);
        res.status(error.status || 500).end(error.message)
      })
  } 
}
export default new Controller();