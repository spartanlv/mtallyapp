import PollingStationsService from '../../services/pollingstations.service';
export class Controller {
  all(req, res) {
    PollingStationsService
      .all()
      .then(r => res.json(r))
      .catch(error => {
        console.log(error);
        res.status(500).end(error.message)
      });
  }

  byId(req, res) {
    PollingStationsService
      .byId(req.params.code)
      .then(r => {
        if (r) res.json(r)
        else res.status(404).end();
      })
      .catch(error => {
        console.log(error);
        res.status(500).end(error.message)
      });
  }

  create(req, res) {
    PollingStationsService
      .create(req.body)
      .then(r => {
        if (r) {
          res
            .status(201)
            .json(r)
        }
      })
      .catch(error => {
        console.log(error.message);
        res.status(500).end(error.message)
      })
  }

  upload(req, res) {
    PollingStationsService
      .upload(req)
      .then(r => {
        if (r) {
          res
            .status(200)
            .json(r)
        }
      })
      .catch(error => {
        console.log(error.message);
        res.status(500).end(error.message)
      })
  }
}
export default new Controller();