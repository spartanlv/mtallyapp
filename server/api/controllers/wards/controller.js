import WardsService from '../../services/wards.service';
export class Controller {
  all(req, res) {
    WardsService
      .all()
      .then(r => res.json(r))
      .catch(error => {
        console.log(error);
        res.status(500).end(error.message)
      });
  }

  byId(req, res) {
    WardsService
      .byId(req.params.code)
      .then(r => {
        if (r) res.json(r)
        else res.status(404).end();
      })
      .catch(error => {
        console.log(error);
        res.status(500).end(error.message)
      });
  }

  create(req, res) {
    WardsService
      .create(req.body)
      .then(r => {
        if (r) {
          res
            .status(201)
            .json(r)
        }
      })
      .catch(error => {
        console.log(error.message);
        res.status(500).end(error.message)
      })
  } 
}
export default new Controller();