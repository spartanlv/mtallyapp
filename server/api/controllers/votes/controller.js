import VotesService from '../../services/votes.service';
export class Controller {
  all(req, res) {
    VotesService
      .all()
      .then(r => res.json(r))
      .catch(error => {
        console.log(error);
        res.status(500).end(error.message)
      });
  }

  byId(req, res) {
    VotesService
      .byId(req.params.candidateId)
      .then(r => {
        if (r) res.json(r)
        else res.status(404).end();
      })
      .catch(error => {
        console.log(error);
        res.status(500).end(error.message)
      });
  }

  byStation(req, res) {
    VotesService
      .byStation(req.params.pollingStationCode)
      .then(r => {
        if (r) res.json(r)
        else res.status(404).end();
      })
      .catch(error => {
        console.log(error);
        res.status(500).end(error.message)
      });
  }

  create(req, res) {
    VotesService
      .create(req)
      .then(r => {
        if (r) {
          res
            .status(201)
            .json(r)
        }
      })
      .catch(error => {
        console.log(error);
        res.status(500).end(error)
      })
  }

  submissionBySuperAgent(req, res) {
    VotesService
      .createBySuperAgent(req)
      .then(r => {
        if (r) {
          res
            .status(201)
            .json(r)
        }
      })
      .catch(error => {
        console.log(error);
        res.status(500).end(error)
      })
  }

}
export default new Controller();