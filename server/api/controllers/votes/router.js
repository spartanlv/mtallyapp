import * as express from 'express';
import controller from './controller'
export default express.Router()
    .post('/', ::controller.create)
    .get('/', ::controller.all)
    .get('/:candidateId', ::controller.byId)
    .get('/pollingstation/:pollingStationCode', ::controller.byStation)
    .post('/pollingstation/', ::controller.submissionBySuperAgent)