import UsersService from '../../services/users.service';
export class Controller {
  login(req, res) {
    UsersService
      .login(req.body)
      .then(r => res.status(200).send(r))
      .catch(error => {
        console.log(error);
        res.status(error.status || 500).end(error.message)
      });
  }

  all(req, res) {
    UsersService
      .all()
      .then(r => res.json(r))
      .catch(error => {
        console.log(error);
        res.status(error.status || 500).end(error.message)
      });
  }

  byId(req, res) {
    UsersService
      .byId(req.params.idNo)
      .then(r => {
        if (r) res.json(r)
        else res.status(404).end();
      })
      .catch(error => {
        console.log(error);
        res.status(error.status || 500).end(error.message)
      });
  }

  create(req, res) {
    UsersService
      .create(req.body)
      .then(r => {
        if (r) {
          res
            .status(201)
            .json(r)
        }
      })
      .catch(error => {
        console.log(error);
        res.status(error.status || 500).end(error.message)
      })
  } 
}
export default new Controller();