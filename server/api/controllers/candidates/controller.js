import CandidatesService from '../../services/candidates.service';
export class Controller {
  all(req, res) {
    CandidatesService
      .all()
      .then(r => res.json(r))
      .catch(error => {
        console.log(error);
        res.status(500).end(error.message)
      });
  }

  byId(req, res) {
    CandidatesService
      .byId(req.params.id)
      .then(r => {
        if (r) res.json(r)
        else res.status(404).end();
      })
      .catch(error => {
        console.log(error);
        res.status(500).end(error.message)
      });
  }

  create(req, res) {
    CandidatesService
      .create(req.body)
      .then(r => {
        if (r) {
          res
            .status(201)
            .json(r)
        }
      })
      .catch(error => {
        console.log(error.message);
        res.status(500).end(error.message)
      })
  } 
}
export default new Controller();