import l from '../../common/logger';
import models from  '../../../db/models';

export class PollingStationsService {
  all() {
    l.info(`${this.constructor.name}.all()`);
    return models.PollingStations
      .findAll({
        attributes: ['code', 'name', 'ward_code', 'total_voters', 'picture', 'rejected_votes', 'total_votes_cast',
          'created_at', 'updated_at', 'deleted_at']
      })
      .then(pollingstations => Promise.resolve(pollingstations))
      .catch(error => Promise.reject(error));
  }

  byId(code) {
    l.info(`${this.constructor.name}.byId(${code})`);
    return models.PollingStations
      .find({
        where: {
          code: code
        },
        attributes: ['code', 'name', 'ward_code', 'total_voters', 'picture', 'rejected_votes', 'total_votes_cast',
          'created_at', 'updated_at', 'deleted_at']
      })
      .then(pollingstation => Promise.resolve(pollingstation))
      .catch(error => Promise.reject(error));
  }

  create(body) {
    l.info(`${this.constructor.name}.create(${body.name})`);

    return models.PollingStations
      .create(body, { fields: Object.keys(body) })
      .then(pollingstation => Promise.resolve(pollingstation))
      .catch(error => Promise.reject(error));
  }

  upload(req) {
    l.info(`${this.constructor.name}.upload(${req.body.picture})`);
    return models.PollingStations
      .update({
          picture: req.body.picture,
          rejectedVotes: req.body.rejectedVotes,
          totalVotesCast: req.body.totalVotesCast
        },
        {
          where: {
            code: req.userPollingStationCode
          },
          returning: true,
          plain: true,
        })
      .then(pollingstation => Promise.resolve(pollingstation))
      .catch(error => Promise.reject(error));
  }
}

export default new PollingStationsService();