import l from '../../common/logger';
import models from  '../../../db/models';


export class VotesService {
  all() {
    l.info(`${this.constructor.name}.all()`);
    return models.Votes
      .findAll({
        order: ["created_at"],
      })
      .then(votes => Promise.resolve(votes))
      .catch(error => Promise.reject(error));

  }

  byId(candidateId) {
    l.info(`${this.constructor.name}.byId(${candidateId})`);
    return models.Votes
      .find({
        where: {
          candidateId: candidateId
        },
        order: ["created_at"],
        // attributes: ['id', 'candidate_id', 'polling_station_code', 'submitted_by', 'total_votes', 'created_at', 'updated_at', 'deleted_at']
        include: [{
          model: models.PollingStations,
          attribute: ['code', 'name', 'ward_code', 'total_voters', 'picture', 'rejected_votes', 'total_votes_cast', 'created_at', 'updated_at', 'deleted_at'],
          as: 'Centre',
          required: false
        }]
      })
      .then(vote => Promise.resolve(vote))
      .catch(error => Promise.reject(error));
  }

  byStation(pollingStationCode) {
    l.info(`${this.constructor.name}.byStation(${pollingStationCode})`);
    return models.Votes
      .find({
        where: {
          pollingStationCode: pollingStationCode
        },
        order: ["created_at"],
        attributes: ['id', 'candidate_id', 'polling_station_code', 'submitted_by', 'total_votes', 'created_at', 'updated_at', 'deleted_at']
      })
      .then(vote => Promise.resolve(vote))
      .catch(error => Promise.reject(error));
  }

  create(req) {
    l.info(`${this.constructor.name}.create(${req.body.candidateId})`);
    req.body.submittedBy = req.userIdNo;
    req.body.pollingStationCode = req.userPollingStationCode;
    return models.Votes
      .findOne(
        {
          where: {pollingStationCode: req.body.pollingStationCode, submittedBy: req.body.submittedBy, candidateId: req.body.candidateId }
        }
      )
      .then((vote) => {
        if (vote){
          return models.Votes
            .update(
              {
                totalVotes: req.body.totalVotes,
              }, {
                where: {pollingStationCode: req.body.pollingStationCode, submittedBy: req.body.submittedBy, candidateId: req.body.candidateId },
                returning: true
            })
            .then(updatedVote => Promise.resolve(updatedVote[1][0]))
            .catch(error => Promise.reject(error))
        } else {
          return models.Votes
            .create(req.body, { fields: Object.keys(req.body) })
            .then(createdVote => Promise.resolve(createdVote))
            .catch(error => Promise.reject(error));
        }

      })
      .catch(error => Promise.reject(error))
  }

  createBySuperAgent(req) {
    l.info(`${this.constructor.name}.createBySuperAgent(${req.body.candidateId})`);
    req.body.submittedBy = req.userIdNo;
    return models.Votes
      .findOne(
        {
          where: {pollingStationCode: req.body.pollingStationCode, submittedBy: req.body.submittedBy, candidateId: req.body.candidateId }
        }
      )
      .then((vote) => {
        if (vote){
          return models.Votes
            .update(
              {
                totalVotes: req.body.totalVotes,
              }, {
                where: {pollingStationCode: req.body.pollingStationCode, submittedBy: req.body.submittedBy, candidateId: req.body.candidateId },
                returning: true
              })
            .then(updatedVote => Promise.resolve(updatedVote[1][0]))
            .catch(error => Promise.reject(error))
        } else {
          return models.Votes
            .create(req.body, { fields: Object.keys(req.body) })
            .then(createdVote => Promise.resolve(createdVote))
            .catch(error => Promise.reject(error));
        }

      })
      .catch(error => Promise.reject(error))

  }

}

export default new VotesService();
