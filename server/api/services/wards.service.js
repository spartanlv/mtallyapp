import l from '../../common/logger';
import models from  '../../../db/models';


export class WardsService {
  all() {
    l.info(`${this.constructor.name}.all()`);
    return models.Wards
      .findAll({
        include: [{
          model: models.PollingStations,
          attribute: ['code', 'name', 'ward_code', 'total_voters', 'picture', 'rejected_votes', 'total_votes_cast', 'created_at', 'updated_at', 'deleted_at'],
          as: 'Wardstations',
          required: false
        }]
      })
      .then(wards => Promise.resolve(wards))
      .catch(error => Promise.reject(error));
  }

  byId(code) {
    l.info(`${this.constructor.name}.byId(${code})`);
    return models.Wards
      .find({
        where: {
          code: code
        },
        // attributes: ['id', 'idNo', 'firstName', 'lastName', 'isActive',
        //   'isAdmin', 'phone', 'pollingStationCode', 'created_at', 'updated_at', 'deleted_at']
      })
      .then(ward => Promise.resolve(ward))
      .catch(error => Promise.reject(error));
  }

  create(body) {
    l.info(`${this.constructor.name}.create(${body.name})`);

    return models.Wards
      .create(body, { fields: Object.keys(body) })
      .then(ward => Promise.resolve(ward))
      .catch(error => Promise.reject(error));
  }
}

export default new WardsService();