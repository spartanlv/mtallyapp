import l from '../../common/logger';
import models from  '../../../db/models';


export class CandidatesService {
  all() {
    l.info(`${this.constructor.name}.all()`);
    return models.Candidates
      .findAll({
        attributes: ['id', 'firstName', 'lastName', 'party', 'created_at', 'updated_at', 'deleted_at'],
        include: [{
          model: models.Votes,
          required: false
        }]
      })
      .then(candidates => Promise.resolve(candidates))
      .catch(error => Promise.reject(error));

  }

  byId(id) {
    l.info(`${this.constructor.name}.byId(${id})`);
    return models.Candidates
      .find({
        where: {
          id: id
        },
        attributes: ['id', 'firstName', 'lastName', 'party', 'created_at', 'updated_at', 'deleted_at'],
        include: [{
          model: models.Votes,
          required: false
        }]
      })
      .then(candidate => Promise.resolve(candidate))
      .catch(error => Promise.reject(error));
  }

  create(body) {
    l.info(`${this.constructor.name}.create(${body.firstName})`);

    return models.Candidates
      .create(body, { fields: Object.keys(body) })
      .then(candidate => Promise.resolve(candidate))
      .catch(error => Promise.reject(error));
  }
}

export default new CandidatesService();