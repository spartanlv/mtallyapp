import l from '../../common/logger';
import models from '../../../db/models';
import * as _ from 'lodash';
import * as jwt from 'jsonwebtoken';
import * as fs from 'fs';
import * as co from 'co';

/**
 * Generates authentication token.
 *
 * @param {any} user
 * @returns
 */
function generateToken(user) {
  return new Promise((resolve) => {
    const cert = fs.readFileSync('./config/certs/cert', 'utf8'); // get private key
    const token = jwt.sign({
      userIdNo: user.idNo,
      userPollingStationCode: user.pollingStationCode
    }, {
      key: cert,
      passphrase: process.env.PASSPHRASE
    }, {
      algorithm: 'RS256',
      expiresIn: 3600
    });
    resolve(token);
  });
}
export class UsersService {
  login(body) {
    l.info(`${this.constructor.name}.login(${body.idNo})`);
    // check id no and confirm password
    return co(function* () {
      const user = yield models.Users.findOne({
        where: {
          idNo: body.idNo
        }
      });
      if (!user) return Promise.reject({ message: 'Invalid credentials', status: 401 });
      const validatePassword = yield user.verifyPassword(body.password, user.password);
      if (!validatePassword) return Promise.reject({ message: 'Invalid credentials', status: 401 });
      const agent = yield models.Agents.findOne({
        where: {
          idNo: user.idNo
        }
      });
      user.pollingStationCode = agent.pollingStationCode;
      const accessToken = yield generateToken(user);
      return Promise.resolve({ access_token: accessToken, isAdmin: user.isAdmin, firstName: agent.firstName, lastName: agent.lastName });
    });
  }

  all() {
    l.info(`${this.constructor.name}.all()`);
    return models.Users
      .findAll({
        attributes: ['id', 'idNo', 'isActive',
          'isAdmin', 'created_at', 'updated_at', 'deleted_at']
      })
      .then(users => Promise.resolve(users))
      .catch(error => Promise.reject(error));
  }

  byId(idNo) {
    l.info(`${this.constructor.name}.byId(${idNo})`);
    return models.Users
      .find({
        where: {
          idNo: idNo
        },
        attributes: ['id', 'idNo', 'isActive',
          'isAdmin', 'created_at', 'updated_at', 'deleted_at']
      })
      .then(user => Promise.resolve(user))
      .catch(error => Promise.reject(error));
  }

  create(body) {
    l.info(`${this.constructor.name}.create(${body.idNo})`);

    return models.Agents
      .count({
        where: {
          idNo: body.idNo
        }
      })
      .then((count) => {
        if (count === 0) {
          return Promise.reject({ status: 401, message: 'Not Authorized' })
        }
        return models.Users
          .create(body, { fields: Object.keys(body) })
              .then(user => Promise.resolve(user))
              .catch((error) => {
                if (error.name === 'SequelizeUniqueConstraintError') {
                  return Promise.reject({ status: 409, message: 'User with Idno exist please log in' })
                }
                return Promise.reject(error)
              });
      })
      .catch(error => Promise.reject(error))
  }
}

export default new UsersService();
