import l from '../../common/logger';
import models from  '../../../db/models';


export class AgentsService {
  all(req) {
    l.info(`${this.constructor.name}.all()`);
    return models.Agents.findOne({
      where: {
        idNo: req.userIdNo
      },
      attributes: ['idNo', 'isActive', 'isAdmin']
    }).then((user) => {
      if (!user) return Promise.reject({status: 401, message: 'Not Authorized'});
      // check if user requesting is Admin
      if (user.isAdmin && user.isActive) {
        return models.Agents
          .findAll({
            include: [{
              model: models.PollingStations,
              as: 'Station',
              required: false
            }]
          })
          .then(agents => Promise.resolve(agents))
          .catch(error => Promise.reject(error));
      } else {
        return Promise.reject({status: 403, message: 'Forbidden'});
      }

    })
      .catch(error => Promise.reject(error))
  }

  byId(idNo) {
    l.info(`${this.constructor.name}.byId(${idNo})`);
    return models.Agents
      .find({
        where: {
          idNo: idNo
        },
        attributes: ['id', 'idNo', 'firstName', 'lastName', 'isActive',
          'isAdmin', 'phone', 'pollingStationCode', 'created_at', 'updated_at', 'deleted_at']
      })
      .then(agent => Promise.resolve(agent))
      .catch(error => Promise.reject(error));
  }

  create(req) {

    l.info(`${this.constructor.name}.create(${req.body.firstName})`);

    return models.Agents.findOne({
      where: {
        idNo: req.userIdNo
      },
      attributes: ['idNo', 'isActive', 'isAdmin']
    }).then((user) => {
      if (!user) return Promise.reject({status: 401, message: 'Not Authorized'});
      // check if creator is Admin
      if (user.isAdmin && user.isActive) {
        req.body.creatorId = req.userIdNo;
        req.body.isActive = true;
        return models.Agents
          .create(req.body, {fields: Object.keys(req.body)})
          .then(agent => Promise.resolve(agent))
          .catch(error => Promise.reject(error));
      } else {
        return Promise.reject({status: 403, message: 'Forbidden'});
      }

    })
      .catch(error => Promise.reject(error))
  }
}

export default new AgentsService();
