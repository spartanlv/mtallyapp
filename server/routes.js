import examplesRouter from './api/controllers/examples/router'
import agentsRouter from './api/controllers/agents/router'
import candidatesRouter from './api/controllers/candidates/router'
import pollingStationsRouter from './api/controllers/pollingstations/router'
import usersRouter from './api/controllers/users/router'
import votesRouter from './api/controllers/votes/router'
import wardsRouter from './api/controllers/wards/router'
import auth from './common/middleware/auth';
import errorHandler from './common/middleware/errorHandler';

export default function routes(app) {
  app.use('/api/v1/user',usersRouter);
  app.use('/api/v1/examples',examplesRouter);
  app.use(auth);
  app.use('/api/v1/agent',agentsRouter);
  app.use('/api/v1/candidate',candidatesRouter);
  app.use('/api/v1/pollingstation',pollingStationsRouter);
  app.use('/api/v1/votes',votesRouter);
  app.use('/api/v1/ward',wardsRouter);
  app.use(errorHandler);
};