export default function (err, req, res, next) {
    // set locals, only providing error in development
    console.log(err);
    res.locals.message = err.message;
    res.locals.error = process.env.NODE_ENV === 'development' ? err : {};
    // render the error page
    res.status(err.status || 500);
    res.send(err);
    next();
}