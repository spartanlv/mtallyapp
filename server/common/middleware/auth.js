import * as jwt from 'jsonwebtoken';
import * as fs from 'fs';

const cert = fs.readFileSync('./config/certs/cert.pem');

export default function (req, res, next) {
    const token = req.get('Authorization');
    if (!token) return next({ status: 401, message: 'Not authenticated' });
    jwt.verify(token, cert.toString(), { algorithms: ['RS256'], ignoreExpiration: true }, (err, decoded) => {
        if (err) {
            console.log(err);
            return next({ message: err, status: 401 });}
        req.userIdNo = decoded.userIdNo;
        req.userPollingStationCode = decoded.userPollingStationCode;
        return next();
    });
}
