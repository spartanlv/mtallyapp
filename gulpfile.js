const exec = require('child_process').exec;
const gulp = require('gulp');


// sequelize migrate db
gulp.task('db:migrate', () => {
  const migrate = exec('./node_modules/.bin/sequelize db:migrate');
  migrate.stdout.on('data', (data) => {
    process.stdout.write(data);
  });
  migrate.stderr.on('data', (data) => {
    process.stdout.write(data);
  });
  migrate.on('close', (code) => {
    if (code === 0) process.stdout.write('Database migration completed successfully');
    else process.stderr.write('Database migration failed');
  });
});

// undos sequelize migration on db
gulp.task('db:migrate:undo:all', () => {
  const undoMigrate = exec('./node_modules/.bin/sequelize db:migrate:undo:all');
  undoMigrate.stdout.on('data', (data) => {
    process.stdout.write(data);
  });
  undoMigrate.stderr.on('data', (data) => {
    process.stdout.write(data);
  });
  undoMigrate.on('close', (code) => {
    if (code === 0) process.stdout.write('Database migration undo completed successfully');
    else process.stderr.write('Database migration undo failed');
  });
});


// sequelize seed db
gulp.task('db:seed:all', () => {
  exec('./node_modules/.bin/sequelize db:sync');
  const migrate = exec('sequelize db:seed:all');
  migrate.stdout.on('data', (data) => {
    process.stdout.write(data);
  });
  migrate.stderr.on('data', (data) => {
    process.stdout.write(data);
  });
  migrate.on('close', (code) => {
    if (code === 0) process.stdout.write('Database seeding completed succefully');
    else process.stderr.write('Database seeding failed');
  });
});