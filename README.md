# mtallyapp

Vote submission app

## Install It
```
npm install
```

## Setup database connection
create database then rename ```env.example``` to ```.env```
update database connection string the run

```
npm run migrate
```

## Seed database
run npm script to populate your database with wards, polling stations amd candidates data

```
npm run seed
```

## Run It
#### Run in *development* mode:

```
npm run dev
```

#### Run in *production* mode:

```
npm compile
npm start
```

#### Deploy to the Cloud
e.g. CloudFoundry

```
cf push mtallyapp
```

### Try It
* Point you're browser to [http://localhost:3000](http://localhost:3000)
* Invoke the example REST endpoint `curl http://localhost:3000/api/v1/wards`
   
