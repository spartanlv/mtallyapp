'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addConstraint('polling_stations', ['ward_code'], {
      type: 'FOREIGN KEY',
      references: { //Required field
        table: 'wards',
        field: 'code'
      },
      onDelete: 'cascade',
      onUpdate: 'cascade'
    });
  },

  down: function (queryInterface, Sequelize) {
  }
};
