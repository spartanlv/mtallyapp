'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addConstraint('votes', ['candidate_id'], {
      type: 'FOREIGN KEY',
      references: { //Required field
        table: 'candidates',
        field: 'id'
      },
      onDelete: 'cascade',
      onUpdate: 'cascade'
    })
      .then(() => queryInterface.addConstraint('votes', ['polling_station_code'], {
        type: 'FOREIGN KEY',
        references: { //Required field
          table: 'polling_stations',
          field: 'code'
        },
        onDelete: 'cascade',
        onUpdate: 'cascade'

      })
        .then(() => queryInterface.addConstraint('votes', ['submitted_by'], {
          type: 'FOREIGN KEY',
          references: { //Required field
            table: 'agents',
            field: 'id_no'
          },
          onDelete: 'cascade',
          onUpdate: 'cascade'

        })));
  },

  down: function (queryInterface, Sequelize) {
  }
};
