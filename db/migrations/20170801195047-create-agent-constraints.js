'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addConstraint('agents', ['creator_id'], {
      type: 'FOREIGN KEY',
      references: { //Required field
        table: 'users',
        field: 'id_no'
      },
      onDelete: 'no action',
      onUpdate: 'no action'
    })
      .then(() => queryInterface.addConstraint('agents', ['polling_station_code'], {
        type: 'FOREIGN KEY',
        references: { //Required field
          table: 'polling_stations',
          field: 'code'
        },
        onDelete: 'cascade',
        onUpdate: 'cascade'
      }));
  },

  down: function (queryInterface, Sequelize) {
  }
};
