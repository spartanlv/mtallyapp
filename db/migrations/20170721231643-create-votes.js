'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('votes', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      candidateId: {
        allowNull: false,
        type: Sequelize.INTEGER,
        field: 'candidate_id'
      },
      pollingStationCode: {
        allowNull: false,
        type: Sequelize.STRING,
        field: 'polling_station_code'
      },
      submittedBy: {
        allowNull: false,
        type: Sequelize.STRING,
        field: 'submitted_by'
      },
      totalVotes: {
        type: Sequelize.INTEGER,
        allowNull: false,
        field: 'total_votes'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        field: 'created_at'
      },
      updatedAt: {
        type: Sequelize.DATE,
        field: 'updated_at'
      },
      deletedAt: {
        type: Sequelize.DATE,
        field: 'deleted_at'
      }
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('votes');
  }
};