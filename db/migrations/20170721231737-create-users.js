'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('users', {
      idNo: {
        allowNull: false,
        type: Sequelize.STRING,
        unique: true,
        primaryKey: true,
        field: 'id_no'
      },
      password: {
        type: Sequelize.STRING,
        allowNull: false,
        field: 'password'
      },
      isAdmin: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        field: 'is_admin'
      },
      isActive: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        field: 'is_active'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        field: 'created_at'
      },
      updatedAt: {
        type: Sequelize.DATE,
        field: 'updated_at'
      },
      deletedAt: {
        type: Sequelize.DATE,
        field: 'deleted_at'
      }
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('users');
  }
};
