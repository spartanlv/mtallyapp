'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('agents', {
      idNo: {
        allowNull: false,
        type: Sequelize.STRING,
        unique: true,
        primaryKey: true,
        field: 'id_no'
      },
      firstName: {
        type: Sequelize.STRING,
        field: 'first_name'
      },
      lastName: {
        type: Sequelize.STRING,
        field: 'last_name'
      },
      pollingStationCode: {
        allowNull: false,
        type: Sequelize.STRING,
        field: 'polling_station_code'
      },
      creatorId: {
        allowNull: false,
        type: Sequelize.STRING,
        field: 'creator_id'
      },
      phone: {
        type: Sequelize.STRING,
        field: 'phone'
      },
      isAdmin: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        field: 'is_admin'
      },
      isActive: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        field: 'is_active'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        field: 'created_at'
      },
      updatedAt: {
        type: Sequelize.DATE,
        field: 'updated_at'
      },
      deletedAt: {
        type: Sequelize.DATE,
        field: 'deleted_at'
      }
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('agents');
  }
};
