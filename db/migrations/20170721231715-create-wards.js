'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('wards', {
      code: {
        allowNull: false,
        type: Sequelize.STRING,
        primaryKey: true,
        field: 'code'
      },
      name: {
        allowNull: false,
        type: Sequelize.STRING,
        field: 'name'
      },
      totalVoters: {
        type: Sequelize.INTEGER,
        field: 'total_voters'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        field: 'created_at'
      },
      updatedAt: {
        type: Sequelize.DATE,
        field: 'updated_at'
      },
      deletedAt: {
        type: Sequelize.DATE,
        field: 'deleted_at'
      }
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('wards');
  }
};