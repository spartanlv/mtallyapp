'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('polling_stations', {
      code: {
        allowNull: false,
        type: Sequelize.STRING,
        primaryKey: true,
        field: 'code'
      },
      name: {
        allowNull: false,
        type: Sequelize.STRING,
        field: 'name'
      },
      wardCode: {
        allowNull: false,
        type: Sequelize.STRING,
        field: 'ward_code'
      },
      totalVoters: {
        type: Sequelize.INTEGER,
        field: 'total_voters'
      },
      picture: {
        type: Sequelize.TEXT,
        allowNull: true,
        field: 'picture'
      },
      rejectedVotes: {
        type: Sequelize.INTEGER,
        allowNull: true,
        field: 'rejected_votes'
      },
      totalVotesCast: {
        type: Sequelize.INTEGER,
        allowNull: true,
        field: 'total_votes_cast'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        field: 'created_at'
      },
      updatedAt: {
        type: Sequelize.DATE,
        field: 'updated_at'
      },
      deletedAt: {
        type: Sequelize.DATE,
        field: 'deleted_at'
      }
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('polling_stations');
  }
};