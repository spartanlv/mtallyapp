'use strict';
let moment = require('moment');
const tableName = 'candidates';
const idSequence = 'candidates_id_seq';

module.exports = {
  up: (queryInterface, Sequelize) => {
    let candidates = [
      {
        first_name: "Kipsengeret",
        last_name: "Koros",
        party: "Independent",
        created_at: moment().format()

      },
      {
        first_name: "Justice",
        last_name: "Kemei",
        party: "Jubilee",
        created_at: moment().format()

      },
      {
        first_name: "Enock",
        last_name: "Too",
        party: "Wiper",
        created_at: moment().format(),

      },
      {
        first_name: "Sammy",
        last_name: "Arap Keter",
        party: "Kanu",
        created_at: moment().format()

      },
      {
        first_name: "Kiprotich",
        last_name: "Rogony",
        party: "Independent",
        created_at: moment().format()

      },
      {
        first_name: "Dr Wilson",
        last_name: "Soy",
        party: "Independent",
        created_at: moment().format()

      },
      {
        first_name: "Jack",
        last_name: "Kipchumba Ngetich",
        party: "Maendeleo Chap Chap",
        created_at: moment().format()

      }
    ];
    return queryInterface.bulkInsert(tableName, candidates, {}).then(() => {
      return queryInterface.sequelize.query(
        `ALTER SEQUENCE ${idSequence}
           RESTART WITH ${candidates.length + 1}`
      );
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(tableName, null, {});
  }
};
