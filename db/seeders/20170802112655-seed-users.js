'use strict';

const tableName = 'users';
const bcrypt = require('bcrypt');
const saltRounds = parseInt(process.env.SALT_ROUNDS, 10);
function hashPassword(user) {
  return bcrypt.hash(user.password, saltRounds).then((hash) => {
    user.password = hash; // eslint-disable-line no-param-reassign
    return user;
  });
}
module.exports = {
  up: function (queryInterface, Sequelize) {
    let user ={
      id_no: process.env.SUPER_AGENT_ID,
      password: process.env.SUPER_AGENT_PASS
    };

    return hashPassword(user).then((user) => {
      return queryInterface.sequelize.query(
        `INSERT INTO users VALUES ( ${user.id_no}, '${user.password}', TRUE, TRUE, now())`
      );

    });
  },

  down: function (queryInterface, Sequelize) {
     return queryInterface.bulkDelete(tableName, null, {});
  }
};
