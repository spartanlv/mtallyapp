'use strict';
let moment = require('moment');

const tableName = 'polling_stations';
module.exports = {
  up: function (queryInterface, Sequelize) {
    let pollingstations = [
      {
        code: "001",
        name: "KOILSIR PRIMARY SCHOOL",
        ward_code: "01",
        created_at: moment().format()


      },
      {
        code: "002",
        name: "CHEPTUIYET PRIMARY SCHOOL",
        ward_code: "01",
        created_at: moment().format()


      },
      {
        code: "003",
        name: "LELAGOI PRIMARY SCHOOL",
        ward_code: "01",
        created_at: moment().format()

      },
      {
        code: "004",
        name: "KIBIRIRGUT PRIMARY SCHOOL",
        ward_code: "01",
        created_at: moment().format()

      },
      {
        code: "005",
        name: "MINDILILWET PRIMARY SCHOOL",
        ward_code: "01",
        created_at: moment().format()

      },
      {
        code: "006",
        name: "KEBENETI PRIMARY SCHOOL",
        ward_code: "01",
        created_at: moment().format()

      },
      {
        code: "007",
        name: "MWEBE PRIMARY SCHOOL",
        ward_code: "01",
        created_at: moment().format()

      },
      {
        code: "008",
        name: "MAEMBA PRIMARY SCHOOL",
        ward_code: "01",
        created_at: moment().format()

      },
      {
        code: "009",
        name: "KAMAGET PRIMARY SCHOOL",
        ward_code: "01",
        created_at: moment().format()

      },
      {
        code: "010",
        name: "CHEMEGONG PRIMARY SCHOOL",
        ward_code: "01",
        created_at: moment().format()

      },
      {
        code: "011",
        name: "KAPKEBURU PRIMARY SCHOOL",
        ward_code: "01",
        created_at: moment().format()

      },
      {
        code: "012",
        name: "MARUMBASI  PRIMARY SCHOOL",
        ward_code: "01",
        created_at: moment().format()

      },
      {
        code: "013",
        name: "KAKIBEI PRIMARY SCHOOL",
        ward_code: "01",
        created_at: moment().format()

      },
      {
        code: "014",
        name: "KALYET PRIMARY SCHOOL",
        ward_code: "01",
        created_at: moment().format()

      },
      {
        code: "015",
        name: "TAIYWET PRIMARY SCHOOL",
        ward_code: "01",
        created_at: moment().format()

      },
      {
        code: "016",
        name: "TONONGOI PRIMARY SCHOOL",
        ward_code: "01",
        created_at: moment().format()

      },
      {
        code: "017",
        name: "KIMALAL PRIMARY SCHOOL",
        ward_code: "01",
        created_at: moment().format()

      },
      {
        code: "018",
        name: "KAPSEWA PRIMARY SCHOOL",
        ward_code: "01",
        created_at: moment().format()

      },
      {
        code: "019",
        name: "KIPSAMOI SECONDARY SCHOOL",
        ward_code: "01",
        created_at: moment().format()

      },
      {
        code: "020",
        name: "CHEPTARIT TEA BUYING CENTRE",
        ward_code: "01",
        created_at: moment().format()

      },
      {
        code: "021",
        name: "KIPKOK PRIMARY SCHOOL",
        ward_code: "02",
        created_at: moment().format()

      },
      {
        code: "022",
        name: "SONDU PRIMARY SCHOOL",
        ward_code: "02",
        created_at: moment().format()

      },
      {
        code: "023",
        name: "SUMEK PRIMARY SCHOOL",
        ward_code: "02",
        created_at: moment().format()

      },
      {
        code: "024",
        name: "CHEPKEMEL PRIMARY SCHOOL",
        ward_code: "02",
        created_at: moment().format()

      },
      {
        code: "025",
        name: "SOSUR NURSERY SCHOOL",
        ward_code: "02",
        created_at: moment().format()

      },
      {
        code: "026",
        name: "MUSARIA PRIMARY SCHOOL",
        ward_code: "02",
        created_at: moment().format()

      },
      {
        code: "027",
        name: "CHEPTAGUM PRIMARY SCHOOL",
        ward_code: "02",
        created_at: moment().format()

      },
      {
        code: "028",
        name: "KOIYAT TEA BUYING CENTRE",
        ward_code: "02",
        created_at: moment().format()

      },
      {
        code: "029",
        name: "SINGORONIK PRIMARY SCHOOL",
        ward_code: "02",
        created_at: moment().format()

      },
      {
        code: "030",
        name: "KAPKOCHEI PRIMARY SCHOOL",
        ward_code: "02",
        created_at: moment().format()

      },
      {
        code: "031",
        name: "KIPTERE VILLAGE POLYTECHNIC",
        ward_code: "02",
        created_at: moment().format()

      },
      {
        code: "032",
        name: "KALYANGWET PRIMARY SCHOOL",
        ward_code: "02",
        created_at: moment().format()

      },
      {
        code: "033",
        name: "KIPLELGUTIK PRIMARY SCHOOL",
        ward_code: "02",
        created_at: moment().format()

      },
      {
        code: "034",
        name: "IRAA PRIMARY SCHOOL",
        ward_code: "02",
        created_at: moment().format()

      },
      {
        code: "035",
        name: "KIMOROGO PRIMARY SCHOOL",
        ward_code: "02",
        created_at: moment().format()

      },
      {
        code: "036",
        name: "KIMASAT PRIMARY SCHOOL",
        ward_code: "02",
        created_at: moment().format()

      },
      {
        code: "037",
        name: "EMDIT PRIMARY SCHOOL",
        ward_code: "02",
        created_at: moment().format()

      },
      {
        code: "038",
        name: "KAPLELARTET PRIMARY SCHOOL",
        ward_code: "02",
        created_at: moment().format()

      },
      {
        code: "039",
        name: "TABAITA PRIMARY SCHOOL",
        ward_code: "02",
        created_at: moment().format()

      },
      {
        code: "040",
        name: "BARNGOROR PRIMARY SCHOOL",
        ward_code: "02",
        created_at: moment().format()

      },
      {
        code: "041",
        name: "KAPSOMBOCH PRIMARY SCHOOL",
        ward_code: "02",
        created_at: moment().format()

      },
      {
        code: "042",
        name: "KAPSOROK PRIMARY SCHOOL",
        ward_code: "03",
        created_at: moment().format()

      },
      {
        code: "043",
        name: "MOTERO PRIMARY SCHOOL",
        ward_code: "03",
        created_at: moment().format()

      },
      {
        code: "044",
        name: "KOIRIR NURSERY SCHOOL",
        ward_code: "03",
        created_at: moment().format()

      },
      {
        code: "045",
        name: "SOLIAT PRIMARY SCHOOL",
        ward_code: "03",
        created_at: moment().format()

      },
      {
        code: "046",
        name: "KAPSEGUT  PRIMARY SCHOOL",
        ward_code: "03",
        created_at: moment().format()

      },
      {
        code: "047",
        name: "TUIYOBEI PRIMARY SCHOOL",
        ward_code: "03",
        created_at: moment().format()

      },
      {
        code: "048",
        name: "KABOKYEK PRIMARY SCHOOL",
        ward_code: "03",
        created_at: moment().format()

      },
      {
        code: "049",
        name: "SOMBICHO PRIMARY SCHOOL",
        ward_code: "03",
        created_at: moment().format()

      },
      {
        code: "050",
        name: "KONGEREN PRIMARY SCHOOL",
        ward_code: "03",
        created_at: moment().format()

      },
      {
        code: "051",
        name: "KAMASEGA PRIMARY SCHOOL",
        ward_code: "03",
        created_at: moment().format()

      },
      {
        code: "052",
        name: "CHEPYEGON PRIMARY SCHOOL",
        ward_code: "03",
        created_at: moment().format()

      },
      {
        code: "053",
        name: "CHERAMOR PRIMARY SCHOOL",
        ward_code: "03",
        created_at: moment().format()

      },
      {
        code: "054",
        name: "KIPTUGUMO PRIMARY SCHOOL",
        ward_code: "03",
        created_at: moment().format()

      },
      {
        code: "055",
        name: "KAITUI PRIMARY SCHOOL",
        ward_code: "03",
        created_at: moment().format()

      },
      {
        code: "056",
        name: "KAPSENWO PRIMARY SCHOOL",
        ward_code: "03",
        created_at: moment().format()

      },
      {
        code: "057",
        name: "KAPKIGORO PRIMARY SCHOOL",
        ward_code: "04",
        created_at: moment().format()

      },
      {
        code: "058",
        name: "SIMBI PRIMARY SCHOOL",
        ward_code: "04",
        created_at: moment().format()

      },
      {
        code: "059",
        name: "NYALILBUCH PRIMARY SCHOOL",
        ward_code: "04",
        created_at: moment().format()

      },
      {
        code: "060",
        name: "NYABERI PRIMARY SCHOOL",
        ward_code: "04",
        created_at: moment().format()

      },
      {
        code: "061",
        name: "KAPTALAMWA PRIMARY SCHOOL",
        ward_code: "04",
        created_at: moment().format()

      },
      {
        code: "062",
        name: "KABORE PRIMARY SCHOOL",
        ward_code: "04",
        created_at: moment().format()

      },
      {
        code: "063",
        name: "CHEPTILILIK PRIMARY SCHOOL",
        ward_code: "04",
        created_at: moment().format()

      },
      {
        code: "064",
        name: "KEJIRIET PRIMARY SCHOOL",
        ward_code: "04",
        created_at: moment().format()

      },
      {
        code: "065",
        name: "SERTWET NURSERY SCHOOL",
        ward_code: "04",
        created_at: moment().format()

      },
      {
        code: "066",
        name: "KAPKORMOM PRIMARY SCHOOL",
        ward_code: "04",
        created_at: moment().format()

      },
      {
        code: "067",
        name: "LAITIGO PRIMARY SCHOOL",
        ward_code: "04",
        created_at: moment().format()

      },
      {
        code: "068",
        name: "KOYABEI PRIMARY SCHOOL",
        ward_code: "04",
        created_at: moment().format()

      },
      {
        code: "069",
        name: "KOITABUROT PRIMARY SCHOOL",
        ward_code: "04",
        created_at: moment().format()

      },
      {
        code: "070",
        name: "KAPCHEBWAI PRIMARY SCHOOL",
        ward_code: "04",
        created_at: moment().format()

      },
      {
        code: "071",
        name: "BOGWO PRIMARY SCHOOL",
        ward_code: "04",
        created_at: moment().format()

      },
      {
        code: "072",
        name: "KIPSITET PRIMARY SCHOOL",
        ward_code: "04",
        created_at: moment().format()

      },
      {
        code: "073",
        name: "MOI KIPSITET SECONDARY",
        ward_code: "04",
        created_at: moment().format()

      },
      {
        code: "074",
        name: "CHEBIRIR PRIMARY SCHOOL",
        ward_code: "04",
        created_at: moment().format()

      },
      {
        code: "075",
        name: "THESALIA PRIMARY SCHOOL",
        ward_code: "04",
        created_at: moment().format()

      },
      {
        code: "076",
        name: "NGOMWET CO-OPERATIVE SOCIETY HALL",
        ward_code: "04",
        created_at: moment().format()

      },
      {
        code: "077",
        name: "KAPLELACH PRIMARY SCHOOL",
        ward_code: "04",
        created_at: moment().format()

      },
      {
        code: "078",
        name: "KEBIMBIR PRIMARY SCHOOL",
        ward_code: "04",
        created_at: moment().format()

      }
    ];

    return queryInterface.bulkInsert(tableName, pollingstations, {}).then(() => {
    });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.bulkDelete(tableName, null, {});
  }
};
