'use strict';

let moment = require('moment');

const tableName = 'agents';
module.exports = {
  up: function (queryInterface, Sequelize) {
    let agents =[{
      id_no: process.env.SUPER_AGENT_ID,
      first_name: process.env.SUPER_AGENT_FNAME,
      last_name: process.env.SUPER_AGENT_LNAME,
      is_admin: true,
      is_active: true,
      creator_id: process.env.SUPER_AGENT_ID,
      phone: process.env.SUPER_AGENT_PHONE,
      polling_station_code: process.env.SUPER_AGENT_STATION,
      created_at: moment().format()
    }];

    return queryInterface.bulkInsert(tableName, agents, {}).then(() => {
    });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.bulkDelete(tableName, null, {});
  }
};
