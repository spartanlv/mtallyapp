'use strict';
let moment = require('moment');

const tableName = 'wards';
module.exports = {
  up: function (queryInterface, Sequelize) {
    let wards = [
      {
        code: "01",
        name: "SIGOWET WARD",
        created_at: moment().format()

      },
      {
        code: "02",
        name: "KAPLELARTET WARD",
        created_at: moment().format()
      },
      {
        code: "03",
        name: "SOLIAT WARD",
        created_at: moment().format()
      },
      {
        code: "04",
        name: "SOIN WARD",
        created_at: moment().format()
      }
    ];

    return queryInterface.bulkInsert(tableName, wards, {}).then(() => {
    });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.bulkDelete(tableName, null, {});
  }
};
