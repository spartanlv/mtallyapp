'use strict';
module.exports = function(sequelize, DataTypes) {
  const Candidates = sequelize.define('Candidates', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      field: 'id'
    },
    firstName: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'first_name'
    },
    lastName: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'last_name'
    },
    party: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'party'
    }
  }, {
    timestamps: true,
    paranoid: true,
    underscored: true,
    freezeTableName: true,
    tableName: 'candidates'
  });

  Candidates.associate = function (models) {
    Candidates.hasMany(models.Votes, {
      foreignKey: 'candidateId',
      sourceKey: 'id',
      onDelete: 'CASCADE',
    })
  };
  return Candidates;
};