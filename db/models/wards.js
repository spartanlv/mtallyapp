'use strict';
module.exports = function(sequelize, DataTypes) {
  const Wards = sequelize.define('Wards', {
    code: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
      primaryKey: true,
      field: 'code'
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'name'
    },
    totalVoters: {
      type: DataTypes.INTEGER,
      field: 'total_voters'
    }
  }, {
    timestamps: true,
    paranoid: true,
    underscored: true,
    freezeTableName: true,
    tableName: 'wards'
  });

  Wards.associate = function (models) {
    Wards.hasMany(models.PollingStations, {
      foreignKey: 'wardCode',
      as: 'Wardstations',
      sourceKey: 'code',
      onDelete: 'CASCADE',
    })
  };
  return Wards;
};