'use strict';
const bcrypt = require('bcrypt');
const saltRounds = parseInt(process.env.SALT_ROUNDS, 10);
/**
 * This function hashed the password contained in the user instance
 *
 * @param {any} user The User instance
 * @returns
 */
function hashPassword(user) {
  return bcrypt.hash(user.password, saltRounds).then((hash) => {
    user.password = hash; // eslint-disable-line no-param-reassign
  });
}

module.exports = function(sequelize, DataTypes) {
  const Users = sequelize.define('Users', {
    idNo: {
      type: DataTypes.STRING,
      primaryKey:true,
      unique: true,
      allowNull: false,
      field: 'id_no'
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'password'
    },
    isAdmin: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      field: 'is_admin'
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      field: 'is_active'
    },
  }, {
    timestamps: true,
    paranoid: true,
    underscored: true,
    freezeTableName: true,
    tableName: 'users'
  });
  Users.associate = function (models) {
    Users.hasMany(models.Agents, {
      foreignKey: 'creatorId',
      as: 'Agents',
      sourceKey: 'idNo',
      onDelete: 'NO ACTION',
    });
  };
  Users.prototype.verifyPassword = function (password, userpaswords) {
    return bcrypt.compare(password, userpaswords).then(r => {
      return r;
    });
  }
  Users.beforeCreate((user, options) => {
    return hashPassword(user);
  });
  return Users;
};
