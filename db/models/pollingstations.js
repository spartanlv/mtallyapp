'use strict';
module.exports = function(sequelize, DataTypes) {
  const PollingStations = sequelize.define('PollingStations', {
    code: {
      type: DataTypes.STRING,
      unique:true,
      allowNull: false,
      primaryKey: true,
      field: 'code'
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'name'
    },
    wardCode: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'ward_code'
    },
    totalVoters: {
      type: DataTypes.INTEGER,
      field: 'total_voters'
    },
    picture: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: 'picture'
    },
    rejectedVotes: {
      type: DataTypes.INTEGER,
      allowNull: true,
      field: 'rejected_votes'
    },
    totalVotesCast: {
      type: DataTypes.INTEGER,
      allowNull: true,
      field: 'total_votes_cast'
    }
  }, {
    timestamps: true,
    paranoid: true,
    underscored: true,
    freezeTableName: true,
    tableName: 'polling_stations'
  });
  PollingStations.associate = function (models) {
    PollingStations.hasMany(models.Agents, {
      foreignKey: 'pollingStationCode',
      as: 'Station',
      sourceKey: 'code',
      onDelete: 'CASCADE',
    });

    PollingStations.belongsTo(models.Wards, {
      foreignKey: {
        name: 'wardCode',
        field: 'ward_code'
      },
      as: 'ward',
      targetKey: 'code',
      onDelete: 'CASCADE',
    })
  };

  return PollingStations;
};
