'use strict';
module.exports = function(sequelize, DataTypes) {
  const Votes = sequelize.define('Votes', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      field: 'id'
    },
    candidateId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'candidate_id'
    },
    pollingStationCode: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'polling_station_code'
    },
    submittedBy: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'submitted_by'
    },
    totalVotes: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'total_votes'
    }
  }, {
    timestamps: true,
    paranoid: true,
    underscored: true,
    freezeTableName: true,
    tableName: 'votes'
  });

  Votes.associate = function (models) {
    Votes.belongsTo(models.Agents, {
      foreignKey: {
        name: 'submittedBy',
        field: 'submitted_by'
      },
      as: 'Agent',
      targetKey: 'idNo',
      onDelete: 'CASCADE',
    });

    Votes.belongsTo(models.Candidates, {
      foreignKey: {
        name: 'candidateId',
        field: 'candidate_id'
      },
      as: 'Candidate',
      targetKey: 'id',
      onDelete: 'CASCADE',
    });

    Votes.belongsTo(models.PollingStations, {
      foreignKey: {
        name: 'PollingStationCode',
        field: 'polling_station_code'
      },
      as: 'Centre',
      targetKey: 'code',
      onDelete: 'CASCADE',
    });
  };

  return Votes;
};