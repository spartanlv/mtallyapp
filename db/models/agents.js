'use strict';
module.exports = function(sequelize, DataTypes) {
  const Agents = sequelize.define('Agents', {
    idNo: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true,
      field: 'id_no'
    },
    firstName: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'first_name'
    },
    lastName: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'last_name'
    },
    pollingStationCode: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'polling_station_code'
    },
    creatorId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'creator_id'
    },
    phone: {
      type: DataTypes.STRING,
      field: 'phone'
    },
    isAdmin: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      field: 'is_admin'
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      field: 'is_active'
    },

  }, {
    timestamps: true,
    paranoid: true,
    underscored: true,
    freezeTableName: true,
    tableName: 'agents'
  });
  Agents.associate = function (models) {
    Agents.belongsTo(models.Users, {
      foreignKey: {
        name: 'creatorId',
        field: 'creator_id'
      },
      as: 'User',
      targetKey: 'idNo',
      onDelete: 'NO ACTION',
    });
    Agents.belongsTo(models.PollingStations, {
      foreignKey: {
        name: 'pollingStationCode',
        field: 'polling_station_code'
      },
      as: 'Station',
      targetKey: 'code',
      onDelete: 'CASCADE',
    });

    Agents.hasMany(models.Votes, {
      foreignKey: 'submittedBy',
      as: 'Votes',
      sourceKey: 'idNo',
      onDelete: 'CASCADE',
    });

  };
  return Agents;
};
